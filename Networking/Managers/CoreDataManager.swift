//
//  CoreManager.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 1.02.22.
//

import Foundation
import CoreData


class CoreDataManager {
    
    static let shared = CoreDataManager()
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "CoreModel")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to h andle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    private init(){}
    
    func getAllUsers(_ completionHandler: @escaping ([User]) -> Void) {
        
        persistentContainer.viewContext.perform {
            
            let userEntities = try? UserEntity.all(context: self.persistentContainer.viewContext)
            let dbUsers = userEntities?.map({User(entity: $0)})
            
            completionHandler(dbUsers ?? [])
        }
    }
    
    func save(_ users: [User], completionHandler: @escaping () -> Void) {
        
        let viewContext = persistentContainer.viewContext
        viewContext.perform {
            for user in users {
                _ = try? UserEntity.findOrCreate(user, context: viewContext)
            }
            try? viewContext.save()
            completionHandler()
        }
    }
    
    func postCreateUser(_ user: User, completionHandler: @escaping () -> Void) {
        
        let viewContext = persistentContainer.viewContext
        viewContext.perform {
            _ = try? UserEntity.findOrCreate(user, context: viewContext)
        }
        try? viewContext.save()
        completionHandler()
    }
    
    func putUser (_ user: User, completionHandler: @escaping (User) -> Void){
        
        let viewContext = persistentContainer.viewContext
        viewContext.perform {
            _ = try? UserEntity.findAndUpdate(user, context: viewContext)
        }
        try? viewContext.save()
        completionHandler(user)
    }
    
    func deleteUser(_ user: User, completionHandler: @escaping (Bool) -> Void) {
        
        let viewContext = persistentContainer.viewContext
        viewContext.perform {
            _ = try? UserEntity.findAndDelete(user, context: viewContext)
        }
        try? viewContext.save()
        completionHandler(true)
    }
    
    func getPostsBy(userId: Int, complitionHandler: @escaping ([Post]) -> Void) {
        let viewContext = persistentContainer.viewContext
        viewContext.perform {
            let postEntities = try? PostEntity.all(userId, context: viewContext)
            let dbPosts = postEntities?.map({Post(entity: $0)})
            
            complitionHandler(dbPosts ?? [])
        }
    }
    
    func savePosts(_ posts: [Post], completionHandler: @escaping () -> Void) {
        
        let viewContext = persistentContainer.viewContext
        viewContext.perform {
            for post in posts {
           
                _ = try? PostEntity.findOrCreate(post, context: viewContext)
            }
            try? viewContext.save()
            completionHandler()
        }
    }
    
    func postCreatePost(_ post: Post, complitionHandler: @escaping (Post) -> Void) {
        
        let viewContext = persistentContainer.viewContext
        viewContext.perform {
            _ = try? PostEntity.findOrCreate(post, context: viewContext)
        }
        try? viewContext.save()
        complitionHandler(post)
    }
    
    func putPost (_ post: Post, complitionHandler: @escaping (Post) -> Void) {
        
        let viewContext = persistentContainer.viewContext
        viewContext.perform {
            _ = try? PostEntity.findAndUpdate(post, context: viewContext)
        }
        try? viewContext.save()
        complitionHandler(post)
    }
    
    func deletePost (_ post: Post, completionHandler: @escaping (Bool) -> Void) {
        
        let viewContext = persistentContainer.viewContext
        viewContext.perform {
            _ = try? PostEntity.findAndDelete(post, context: viewContext)
        }
        try? viewContext.save()
        completionHandler(true)
    }
    
    func saveComment(_ comments: [Comment], completionHandler: @escaping () -> Void) {
        
        let viewContext = persistentContainer.viewContext
        viewContext.perform {
            for comment in comments {
                _ = try? CommentEntity.findOrCreate(comment, context: viewContext)
            }
            try? viewContext.save()
            completionHandler()
        }
    }
    
    func postCreateComment(_ comment: Comment, complitionHandler: @escaping (Comment) -> Void) {
        
        let viewContext = persistentContainer.viewContext
        viewContext.perform {
            _ = try? CommentEntity.findOrCreate(comment, context: viewContext)
        }
        try? viewContext.save()
        complitionHandler(comment)
    }
    
    func putComment (_ comment: Comment, complitionHandler: @escaping (Comment) -> Void) {
        
        let viewContext = persistentContainer.viewContext
        viewContext.perform {
            _ = try? CommentEntity.findAndUpdate(comment, context: viewContext)
        }
        try? viewContext.save()
        complitionHandler(comment)
    }
    
    func deleteComment (_ comment: Comment, completionHandler: @escaping (Bool) -> Void) {
        
        let viewContext = persistentContainer.viewContext
        viewContext.perform {
            _ = try? CommentEntity.findAndDelete(comment, context: viewContext)
        }
        try? viewContext.save()
        completionHandler(true)
    }
    
    func getCommentsBy(postId: Int, complitionHandler: @escaping ([Comment]) -> Void) {
        let viewContext = persistentContainer.viewContext
        viewContext.perform {
            let commentEntities = try? CommentEntity.all(postId, context: viewContext)
            let dbComments = commentEntities?.map({Comment(entity: $0)})
            
            complitionHandler(dbComments ?? [])
        }
    }
}
