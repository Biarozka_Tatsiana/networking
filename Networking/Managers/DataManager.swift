//
//  DataManager.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 9.02.22.
//

import Foundation

class DataManager {
    
    let coreDataManager = CoreDataManager.shared
    let networkmanager = NetworkManager()
    
    func getAllUsers (_ completionHandler: @escaping ([User]) -> Void) {
        
        coreDataManager.getAllUsers { (users) in
            
            if users.count > 0 {
                completionHandler(users)
            } else {
                self.networkmanager.getAllUsers { (users) in
                    
                    self.coreDataManager.save(users) {
                        completionHandler(users)
                    }
                }
            }
        }
    }
    
    func postCreateUser (_ user: User, complitionHandler: @escaping (User) -> Void ) {
        coreDataManager.postCreateUser(user) {
            self.networkmanager.postCreateUser(user) { user in
                complitionHandler(user)
            }
        }
    }
    
    func putUser (_ user: User, completionHandler: @escaping (User) -> Void) {
        coreDataManager.putUser(user) { user in
            self.networkmanager.putUser(user) { user in
                completionHandler(user)
            }
        }
    }
    
    func deleteUser (_ user: User, completionHandler: @escaping (Bool) -> Void) {
        coreDataManager.deleteUser(user) { _ in
            self.networkmanager.deleteUser(user) { _ in
                completionHandler(true)
            }
        }
    }
    
    func getPostsBy(userId: Int, completionHandler: @escaping ([Post]) -> Void) {
        
        coreDataManager.getPostsBy(userId: userId) { (posts) in
            if posts.count > 0 {
                completionHandler(posts)
            } else {
                self.networkmanager.getPostsBy(userId: userId) { (posts) in
                    self.coreDataManager.savePosts(posts) {
                        completionHandler(posts)
                    }
                }
            }
        }
    }
    
    func postCreatePost(_ post: Post, complitionHandler: @escaping (Post) -> Void) {
        
        coreDataManager.postCreatePost(post) { post in
            self.networkmanager.postCreatePost(post) { post in
                complitionHandler(post)
            }
        }
    }
    
    func putPost(_ post: Post, complitionHandler: @escaping (Post) -> Void) {
        
        coreDataManager.putPost(post) { post in
            self.networkmanager.putPost(post) { post in
                complitionHandler(post)
            }
        }
    }
    
    func deletePost (_ post: Post, completionHandler: @escaping (Bool) -> Void) {
        
        coreDataManager.deletePost(post) { _ in
            self.networkmanager.deletePost(post) { _ in
                completionHandler(true)
            }
        }
    }
    
    func getCommentsBy(postId: Int, completionHandler: @escaping ([Comment]) -> Void) {
        
        coreDataManager.getCommentsBy(postId: postId) { (comments) in
            if comments.count > 0 {
                completionHandler(comments)
            } else {
                self.networkmanager.getCommentsBy(postId: postId) { (comments) in
                    self.coreDataManager.saveComment(comments) {
                        completionHandler(comments)
                    }
                }
            }
        }
    }
    
    func postCreateComment(_ comment: Comment, complitionHandler: @escaping (Comment) -> Void) {
        
        coreDataManager.postCreateComment(comment) { comment in
            self.networkmanager.postCreateComment(comment) { comment in
                complitionHandler(comment)
            }
        }
    }
    
    func putComment(_ comment: Comment, complitionHandler: @escaping (Comment) -> Void) {
        
        coreDataManager.putComment(comment) { comment in
            self.networkmanager.putComment(comment) { comment in
                complitionHandler(comment)
            }
        }
    }
    
    func deleteComment (_ comment: Comment, completionHandler: @escaping (Bool) -> Void) {
        
        coreDataManager.deleteComment(comment) { _ in
            self.networkmanager.deleteComment(comment) { _ in
                completionHandler(true)
            }
        }
    }
}

