//
//  NetworkManager.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 27.01.22.
//

import Foundation

class NetworkManager {
    
    enum HTTPMethod: String {
        case POST
        case PUT
        case GET
        case DELETE
    }
    
    enum APIs: String {
        case posts
        case users
        case comments
    }
    
    private let baseURL = "https://jsonplaceholder.typicode.com/"
    
    func getAllUsers (_ completionHandler: @escaping ([User]) -> Void) {
        
        if let url = URL(string: baseURL + APIs.users.rawValue) {
            URLSession.shared.dataTask(with: url) { data, responce, error in
                
                if error != nil {
                    print("error in request")
                } else {
                    if let resp = responce as? HTTPURLResponse, resp.statusCode == 200, let responceData = data {
                        let users = try? JSONDecoder().decode([User].self, from: responceData)
                        completionHandler(users ?? [])
                    }
                }
            } .resume()
        }
    }
    
    func postCreateUser (_ user: User, complitionHandler: @escaping (User) -> Void ) {
        
        guard let url = URL(string: baseURL + APIs.users.rawValue), let data = try? JSONEncoder().encode(user) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.POST.rawValue
        request.httpBody = data
        request.setValue("\(data.count)", forHTTPHeaderField: "Content-Lenght")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            if error != nil {
                print("error")
            } else {
                if let resp = response as? HTTPURLResponse, resp.statusCode == 201, let responseData = data {
                    if let responseUser = try? JSONDecoder().decode(User.self, from: responseData) {
                        complitionHandler(responseUser)
                    }
                }
            }
        }.resume()
    }
    
    func deleteUser (_ user: User, completionHandler: @escaping (Bool) -> Void) {
        
        guard let url = URL(string: baseURL + APIs.users.rawValue + "/\(user.id)"), let data = try? JSONEncoder().encode(user) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.DELETE.rawValue
        request.httpBody = data
        request.setValue("\(data.count)", forHTTPHeaderField: "Content-Lenght")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            if error != nil {
                print("error")
            } else {
                if let resp = response as? HTTPURLResponse, resp.statusCode == 200 {
                    completionHandler(true)
                }
            }
        }.resume()
    }
    
    func putUser (_ user: User, complitionHandler: @escaping (User) -> Void) {
        
        guard let url = URL(string: baseURL + APIs.users.rawValue + "/\(user.id)"), let data = try? JSONEncoder().encode(user) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.PUT.rawValue
        request.httpBody = data
        request.setValue("\(data.count)", forHTTPHeaderField: "Content-Lenght")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            if error != nil {
                print("error")
            } else {
                if let resp = response as? HTTPURLResponse, resp.statusCode == 200, let responseData = data {
                    if let responseUser = try? JSONDecoder().decode(User.self, from: responseData) {
                        complitionHandler(responseUser)
                    }
                }
            }
        }.resume()
    }
    
    func postCreatePost(_ post: Post, complitionHandler: @escaping (Post) -> Void) {
        
        guard let url = URL(string: baseURL + APIs.posts.rawValue), let data = try? JSONEncoder().encode(post) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.POST.rawValue
        request.httpBody = data
        request.setValue("\(data.count)", forHTTPHeaderField: "Content-Lenght")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            if error != nil {
                print("error")
            } else if let resp = response as? HTTPURLResponse, resp.statusCode == 201, let responseData = data {
                if let responsePost = try? JSONDecoder().decode(Post.self, from: responseData) {
                    complitionHandler(responsePost)
                }
            }
        }.resume()
    }
    
    func getPostsBy(userId: Int, complitionHandler: @escaping ([Post]) -> Void) {
        
        guard let url = URL(string: baseURL + APIs.posts.rawValue) else { return }
        var components = URLComponents(url: url, resolvingAgainstBaseURL: false)
        components?.queryItems = [URLQueryItem(name: "userId", value: "\(userId)")]
        guard let queryURL = components?.url else { return }
        URLSession.shared.dataTask(with: queryURL) { data, response, error in
            if error != nil {
                print("error getPostsBy")
            } else if let resp = response as? HTTPURLResponse, resp.statusCode == 200, let reciveData = data {
                let posts = try? JSONDecoder().decode([Post].self, from: reciveData)
                
                complitionHandler(posts ?? [])
            }
        }.resume()
    }
    
    func deletePost (_ post: Post, completionHandler: @escaping (Bool) -> Void) {
        
        guard let url = URL(string: baseURL + APIs.posts.rawValue + "/\(post.id)"), let data = try? JSONEncoder().encode(post) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.DELETE.rawValue
        request.httpBody = data
        request.setValue("\(data.count)", forHTTPHeaderField: "Content-Lenght")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            if error != nil {
                print("error")
            } else {
                if let resp = response as? HTTPURLResponse, resp.statusCode == 200 {
                    completionHandler(true)
                }
            }
        }.resume()
    }
    
    func putPost (_ post: Post, complitionHandler: @escaping (Post) -> Void) {
        
        guard let url = URL(string: baseURL + APIs.posts.rawValue + "/\(post.id)"), let data = try? JSONEncoder().encode(post) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.PUT.rawValue
        request.httpBody = data
        request.setValue("\(data.count)", forHTTPHeaderField: "Content-Lenght")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            if error != nil {
                print("error")
            } else {
                if let resp = response as? HTTPURLResponse, resp.statusCode == 200, let responseData = data {
                    if let responsePost = try? JSONDecoder().decode(Post.self, from: responseData) {
                        complitionHandler(responsePost)
                    }
                }
            }
        }.resume()
    }
    
    func postCreateComment(_ comment: Comment, complitionHandler: @escaping (Comment) -> Void) {
        
        guard let url = URL(string: baseURL + APIs.comments.rawValue), let data = try? JSONEncoder().encode(comment) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.POST.rawValue
        request.httpBody = data
        request.setValue("\(data.count)", forHTTPHeaderField: "Content-Lenght")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            if error != nil {
                print("error")
            } else if let resp = response as? HTTPURLResponse, resp.statusCode == 201, let responseData = data {
                if let responseComment = try? JSONDecoder().decode(Comment.self, from: responseData) {
                    complitionHandler(responseComment)
                }
            }
        }.resume()
    }
    
    func getCommentsBy(postId: Int, complitionHandler: @escaping ([Comment]) -> Void) {
        
        guard let url = URL(string: baseURL + APIs.comments.rawValue) else { return }
        var components = URLComponents(url: url, resolvingAgainstBaseURL: false)
        components?.queryItems = [URLQueryItem(name: "postId", value: "\(postId)")]
        guard let queryURL = components?.url else { return }
        URLSession.shared.dataTask(with: queryURL) { data, response, error in
            if error != nil {
                print("error getCommentsBy")
            } else if let resp = response as? HTTPURLResponse, resp.statusCode == 200, let reciveData = data {
                let comments = try? JSONDecoder().decode([Comment].self, from: reciveData)
                
                complitionHandler(comments ?? [])
            }
        }.resume()
    }
    
    func deleteComment (_ comment: Comment, completionHandler: @escaping (Bool) -> Void) {
        
        guard let url = URL(string: baseURL + APIs.comments.rawValue + "/\(comment.id)"), let data = try? JSONEncoder().encode(comment) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.DELETE.rawValue
        request.httpBody = data
        request.setValue("\(data.count)", forHTTPHeaderField: "Content-Lenght")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            if error != nil {
                print("error")
            } else {
                if let resp = response as? HTTPURLResponse, resp.statusCode == 200 {
                    completionHandler(true)
                }
            }
        }.resume()
    }
    
    func putComment (_ comment: Comment, complitionHandler: @escaping (Comment) -> Void) {
        
        guard let url = URL(string: baseURL + APIs.comments.rawValue + "/\(comment.id)"), let data = try? JSONEncoder().encode(comment) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.PUT.rawValue
        request.httpBody = data
        request.setValue("\(data.count)", forHTTPHeaderField: "Content-Lenght")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            if error != nil {
                print("error")
            } else {
                if let resp = response as? HTTPURLResponse, resp.statusCode == 200, let responseData = data {
                    if let responseComment = try? JSONDecoder().decode(Comment.self, from: responseData) {
                        complitionHandler(responseComment)
                    }
                }
            }
        }.resume()
    }
}

