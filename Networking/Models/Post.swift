//
//  Post.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 27.01.22.
//

import Foundation

class Post: Codable {
    
    var userId: Int
    var id: Int
    var title: String
    var body: String
    
    init (userId: Int, title: String, body: String, id: Int) {
        self.userId = userId
        self.title = title
        self.body = body
        self.id = id
    }
    init(entity: PostEntity) {
        self.userId = Int(entity.userId)
        self.title = entity.title ?? ""
        self.body = entity.body ?? ""
        self.id = Int(entity.id)
    }
}
