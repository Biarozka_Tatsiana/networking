//
//  Comment.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 2.02.22.
//

import Foundation

class Comment: Codable {
    var postId: Int
    var id: Int
    var name: String
    var email: String
    var body: String
    
    init(postId: Int, id: Int, name: String, email: String, body: String) {
        self.postId = postId
        self.id = id
        self.name = name
        self.email = email
        self.body = body
    }
    
    init(entity: CommentEntity) {
        
        self.postId = Int(entity.postId)
        self.id = Int(entity.id)
        self.name = entity.name ?? ""
        self.email = entity.email ?? ""
        self.body = entity.body ?? ""
    }
}
