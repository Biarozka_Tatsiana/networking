//
//  User.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 1.02.22.
//

import Foundation

class User: Codable {
    
    var id: Int
    var name: String
    var username: String
    var email: String
    var address: Address?
    var phone: String
    var website: String
    var company: Company?
    
    init(id: Int, name: String, username: String, email: String, phone: String, website: String) {
        self.id = id
        self.name = name
        self.username = username
        self.email = email
        self.address = Address(street: "", suite: "", city: "", zipcode: "", geo: Geo(lat: "", lng: ""))
        self.phone = phone
        self.website = website
        self.company = Company(name: "", catchPhrase: "", bs: "")
        
    }
    init(entity: UserEntity) {
        self.id = Int(entity.id)
        self.name = entity.name ?? ""
        self.username = entity.username ?? ""
        self.email = entity.email ?? ""
        self.phone = entity.phone ?? ""
        self.website = entity.website ?? ""
    }
}

struct Address: Codable {
    var street: String
    var suite: String
    var city: String
    var zipcode: String
    var geo: Geo
}

struct Company: Codable {
    var name: String
    var catchPhrase: String
    var bs: String
}

struct Geo: Codable {
    var lat: String
    var lng: String
}


