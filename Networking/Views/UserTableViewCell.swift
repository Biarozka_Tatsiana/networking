//
//  UserTableViewCell.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 2.02.22.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var usernameUserLabel: UILabel!
    @IBOutlet weak var emailUserLabel: UILabel!
    @IBOutlet weak var phoneUserLabel: UILabel!
    @IBOutlet weak var websiteUserLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    func configure (_ users: User) {
        
        userNameLabel.text = users.name
        usernameUserLabel.text = users.username
        emailUserLabel.text = users.email
        phoneUserLabel.text = users.phone
        websiteUserLabel.text = users.website
    }
}
