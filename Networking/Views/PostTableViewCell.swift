//
//  PostTableViewCell.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 27.01.22.
//

import UIKit

class PostTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure (_ post: Post) {
       
        titleLabel.text = post.title
        bodyLabel.text = post.body
    }
}
