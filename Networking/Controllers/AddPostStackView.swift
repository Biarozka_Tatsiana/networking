//
//  AddPostStackView.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 18.02.22.
//

import UIKit

protocol AddPostStackViewDelegate: AnyObject {
    
    func touchBegan()
}


class AddPostStackView: UIStackView {

    weak var delegate: AddPostStackViewDelegate?
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.delegate?.touchBegan()
    }
}
