//
//  AddUsersViewController.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 3.02.22.
//

import UIKit

protocol AddUsersViewControllerDelegate: AnyObject {
    
    func didFinishCreateUpdate (_ user: User)
}

class AddUsersViewController: UIViewController {
    
    @IBOutlet weak var saveUserButton: UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailTextFeild: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var websiteTextField: UITextField!
    
    
    var networkManager = NetworkManager()
    var dataManager = DataManager()
    var users = [User]()
    var currentUser = User(id: 0, name: "", username: "", email: "", phone: "", website: "")
    
    weak var delegate: AddUsersViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameTextField.delegate = self
        usernameTextField.delegate = self
        emailTextFeild.delegate = self
        phoneTextField.delegate = self
        websiteTextField.delegate = self
        saveUserButton.tintColor = .gray
        showCurrentUser()
    }
    
    
    func showCurrentUser() {
        nameTextField.text = currentUser.name
        usernameTextField.text = currentUser.username
        emailTextFeild.text = currentUser.email
        phoneTextField.text = currentUser.phone
        websiteTextField.text = currentUser.website
    }
    
    func checkTextFields () {
        
        if nameTextField.text?.trimmingCharacters(in: .whitespaces) != nil,  usernameTextField.text?.trimmingCharacters(in: .whitespaces) != nil,
           emailTextFeild.text?.trimmingCharacters(in: .whitespaces) != nil &&
            emailTextFeild.text!.isValidEmail,
           phoneTextField.text?.trimmingCharacters(in: .whitespaces) != nil,
           websiteTextField.text?.trimmingCharacters(in: .whitespaces) != nil {
            self.saveUserButton.isUserInteractionEnabled = true
            self.saveUserButton.tintColor = .systemBlue
        } else {
            self.saveUserButton.isUserInteractionEnabled = false
            self.saveUserButton.tintColor = .gray
            showAlertWrongEmailAndName()
        }
    }
    
    func showAlertWrongEmailAndName() {
        
        let alert = UIAlertController(title: "Name and Email", message: "Your name and email are empty or incorrect", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func createUser() {
        
        guard let name = nameTextField.text else { return }
        guard let username = usernameTextField.text else { return }
        guard let email = emailTextFeild.text else { return }
        guard let phone = phoneTextField.text else { return }
        guard let website = websiteTextField.text else { return }
        
        if currentUser.id != 0 {
            let id = currentUser.id
            let user = User(id: id, name: name, username: username, email: email, phone: phone, website: website)
            dataManager.putUser((user)) { user in
                self.delegate?.didFinishCreateUpdate(user)
            }
        } else {
            let id = 0
            let user = User(id: id, name: name, username: username, email: email, phone: phone, website: website)
            dataManager.postCreateUser(user) { serverUser in
                user.id = serverUser.id
                self.delegate?.didFinishCreateUpdate(user)
            }
        }
        showAlertSuccessCreationUpdating()
    }
    
    func showAlertSuccessCreationUpdating () {
        
        let alert = UIAlertController(title: nil, message: "User has been created or updated successfully", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func saveUserButtonPressed(_ sender: UIButton) {
        
        checkTextFields()
        createUser ()
    }
}

extension AddUsersViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        checkTextFields()
        textField.resignFirstResponder()
        return true
    }
}

