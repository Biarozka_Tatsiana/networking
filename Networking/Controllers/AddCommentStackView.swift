//
//  AddCommentStackView.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 18.02.22.
//

import UIKit

protocol AddCommentStackViewDelegate: AnyObject {
    
    func touchBeganComment()
}

class AddCommentStackView: UIStackView {

    weak var delegate: AddCommentStackViewDelegate?

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.delegate?.touchBeganComment()
    }
}
