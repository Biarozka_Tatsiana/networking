//
//  AddPostsViewController.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 8.02.22.
//

import UIKit

protocol AddPostsViewControllerDelegate: AnyObject {
    
    func didFinishCreateUpdate (_ post: Post)
}

class AddPostsViewController: UIViewController {
    
    @IBOutlet weak var titleTextView: UITextView!
    @IBOutlet weak var bodyTextView: UITextView!
    @IBOutlet weak var savePostButton: UIButton!
    @IBOutlet weak var stackView: AddPostStackView!
    
    var selectedUser = User(id: 0, name: "", username: "", email: "", phone: "", website: "")
    var posts = [Post] ()
    var currentPost = Post(userId: 0, title: "", body: "", id: 0)
    var networkManager = NetworkManager()
    var dataManager = DataManager()
    var borderWidth = 0.7
    var cornerRadius: CGFloat = 7
    
    weak var delegate: AddPostsViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleTextView.delegate = self
        bodyTextView.delegate = self
        stackView.delegate = self
        configureTextView(titleTextView)
        configureTextView(bodyTextView)
        showSavePostButton()
        notifyUpdating()
        showCurrentPost()
    }
    
    func notifyUpdating() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateTextView), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateTextView), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func showSavePostButton() {
        
        savePostButton.tintColor = .gray
        savePostButton.isUserInteractionEnabled = false
    }
    
    func configureTextView (_ textView: UITextView) {
        
        textView.layer.borderWidth = borderWidth
        textView.layer.borderColor = UIColor.lightGray.cgColor
        textView.layer.cornerRadius = cornerRadius
    }
    
    func showCurrentPost() {
        
        titleTextView.text = currentPost.title
        bodyTextView.text = currentPost.body
    }
    
    func showAlertEmptyFields() {
        
        let alert = UIAlertController(title: "", message: "Please, fill in the fields", preferredStyle: .alert)
        let okAcrion = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAcrion)
        present(alert, animated: true, completion: nil)
    }
    
    func createPost() {
        
        guard let title = titleTextView.text else { return }
        guard let body = bodyTextView.text else { return }
        
        if currentPost.id != 0 {
            let id = currentPost.id
            let post = Post(userId: selectedUser.id, title: title, body: body, id: id)
            dataManager.putPost(post) { [weak self] post in
                self?.delegate?.didFinishCreateUpdate(post)
            }
        } else {
            let id = 0
            let post = Post(userId: selectedUser.id, title: title, body: body, id: id)
            dataManager.postCreatePost(post) { [weak self] serverPost in
                post.id = serverPost.id
                self?.delegate?.didFinishCreateUpdate(post)
            }
        }
        showAlertSuccessCreationUpdating()
    }
    
    func showAlertSuccessCreationUpdating () {
        
        let alert = UIAlertController(title: nil, message: "Your post has been created or updated successfully", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc  func updateTextView(parametr: Notification) {
        let userInfo = parametr.userInfo
        let keyboardRect = (userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardFrame = view.convert(keyboardRect, to: view.window)
        if parametr.name == UIResponder.keyboardWillHideNotification {
            titleTextView.contentInset = UIEdgeInsets.zero
            bodyTextView.contentInset = UIEdgeInsets.zero
        } else {
            titleTextView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height / 2, right: 0)
            titleTextView.scrollIndicatorInsets = titleTextView.contentInset
            bodyTextView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height / 2, right: 0)
            bodyTextView.scrollIndicatorInsets = bodyTextView.contentInset
        }
        titleTextView.scrollRangeToVisible(titleTextView.selectedRange)
        bodyTextView.scrollRangeToVisible(bodyTextView.selectedRange)
    }
    
    @IBAction func savePostButtonPressed(_ sender: Any) {
        
        createPost()
    }
}

extension AddPostsViewController: UITextViewDelegate {
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        
        if let text = textView.text, !text.trimmingCharacters(in: .whitespaces).isEmpty {
            self.savePostButton.isUserInteractionEnabled = true
            self.savePostButton.tintColor = .systemBlue
        } else {
            self.savePostButton.isUserInteractionEnabled = false
            self.savePostButton.tintColor = .gray
            showAlertEmptyFields()
        }
        return true
    }
}

extension AddPostsViewController: AddPostStackViewDelegate {
    
    func touchBegan() {
        titleTextView.resignFirstResponder()
        bodyTextView.resignFirstResponder()
    }
}
