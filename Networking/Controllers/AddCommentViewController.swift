//
//  AddCommentViewController.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 13.02.22.
//

import UIKit

protocol AddCommentViewControllerDelegate: AnyObject {
    
    func didFinishCreateUpdate (_ comment: Comment)
}

class AddCommentViewController: UIViewController {
    
    
    @IBOutlet weak var stackView: AddCommentStackView!
    @IBOutlet weak var nameCommentTextView: UITextView!
    @IBOutlet weak var bodyCommentTextView: UITextView!
    @IBOutlet weak var saveCommentButton: UIButton!
    
    var currentComment = Comment(postId: 0, id: 0, name: "", email: "", body: "")
    var selectedPost = Post(userId: 0, title: "", body: "", id: 0)
    var networkManager = NetworkManager()
    var dataManager = DataManager()
    var borderWidth = 0.7
    var cornerRadius: CGFloat = 7
    
    weak var delegate: AddCommentViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameCommentTextView.delegate = self
        bodyCommentTextView.delegate = self
        stackView.delegate = self
        showSaveCommentbutton()
        configureTextView(nameCommentTextView)
        configureTextView(bodyCommentTextView)
        showCurrentComment()
        notifyUpdating()
    }
    
    func notifyUpdating() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateTextView), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateTextView), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func showSaveCommentbutton() {
        
        saveCommentButton.tintColor = .gray
        saveCommentButton.isUserInteractionEnabled = false
    }
    
    func configureTextView (_ textView: UITextView) {
        
        textView.layer.borderWidth = borderWidth
        textView.layer.borderColor = UIColor.lightGray.cgColor
        textView.layer.cornerRadius = cornerRadius
    }
    
    func showCurrentComment() {
        
        nameCommentTextView.text = currentComment.name
        bodyCommentTextView.text = currentComment.body
    }
    
    func showAlertEmptyFields() {
        
        let alert = UIAlertController(title: "", message: "Please, fill in the fields", preferredStyle: .alert)
        let okAcrion = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAcrion)
        present(alert, animated: true, completion: nil)
    }
    
    func createComment() {
        
        guard let name = nameCommentTextView.text else { return }
        guard let body = bodyCommentTextView.text else { return }
        
        if currentComment.id != 0 {
            let id = currentComment.id
            let comment = Comment(postId: selectedPost.id, id: id, name: name, email: "", body: body)
            dataManager.putComment(comment) { [weak self] comment in
                self?.delegate?.didFinishCreateUpdate(comment)
            }
        } else {
            let id = 0
            let comment = Comment(postId: selectedPost.id, id: id, name: name, email: "", body: body)
            dataManager.postCreateComment(comment) { [weak self] serverComment in
                comment.id = serverComment.id
                self?.delegate?.didFinishCreateUpdate(comment)
            }
        }
        showAlertSuccessCreationUpdating()
    }
    
    func showAlertSuccessCreationUpdating () {
        
        let alert = UIAlertController(title: nil, message: "Your comment has been created or updated successfully", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc  func updateTextView(parametr: Notification) {
        let userInfo = parametr.userInfo
        let keyboardRect = (userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardFrame = view.convert(keyboardRect, to: view.window)
        if parametr.name == UIResponder.keyboardWillHideNotification {
            nameCommentTextView.contentInset = UIEdgeInsets.zero
            bodyCommentTextView.contentInset = UIEdgeInsets.zero
        } else {
            nameCommentTextView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height / 2, right: 0)
            nameCommentTextView.scrollIndicatorInsets = nameCommentTextView.contentInset
            bodyCommentTextView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height / 2, right: 0)
            bodyCommentTextView.scrollIndicatorInsets = bodyCommentTextView.contentInset
        }
        nameCommentTextView.scrollRangeToVisible(nameCommentTextView.selectedRange)
        bodyCommentTextView.scrollRangeToVisible(bodyCommentTextView.selectedRange)
    }
    
    @IBAction func saveCommentButtonPressed(_ sender: UIButton) {
        
        createComment()
    }
}

extension AddCommentViewController: UITextViewDelegate, AddCommentStackViewDelegate {
    
    func touchBeganComment() {
        
        nameCommentTextView.resignFirstResponder()
        bodyCommentTextView.resignFirstResponder()
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        
        if let text = textView.text, !text.trimmingCharacters(in: .whitespaces).isEmpty {
            self.saveCommentButton.isUserInteractionEnabled = true
            self.saveCommentButton.tintColor = .systemBlue
        } else {
            self.saveCommentButton.isUserInteractionEnabled = false
            self.saveCommentButton.tintColor = .gray
            showAlertEmptyFields()
        }
        return true
    }
}
