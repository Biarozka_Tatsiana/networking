//
//  UserViewController.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 2.02.22.
//

import UIKit

class UserViewController: UIViewController {
    
    @IBOutlet weak var userTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var networkManager = NetworkManager()
    var dataManager = DataManager()
    var users = [User]() {
        
        didSet {
            DispatchQueue.main.async { [self] in
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
                self.userTableView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userTableView.delegate = self
        userTableView.dataSource = self
        registerСell()
        configureActivityIndicator()
        dataManager.getAllUsers { [weak self] users in
            self?.users = users
        }
        configureRefreshControl()
    }
    
    func registerСell() {
        
        let nib = UINib(nibName: "UserTableViewCell", bundle: nil)
        userTableView.register(nib, forCellReuseIdentifier: "UserTableViewCellID")
    }
    
    func configureActivityIndicator () {
        
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
        activityIndicator.center = view.center
    }
    
    func configureRefreshControl () {
        
        userTableView.refreshControl = UIRefreshControl()
        userTableView.refreshControl?.addTarget(self, action: #selector(handleRefreshControl), for: .valueChanged)
    }
    
    @objc func handleRefreshControl() {
       
        dataManager.getAllUsers { [weak self] users in

            self?.users = users
            self?.userTableView.refreshControl?.endRefreshing()
        }
    }
    
    func saveUser(_ user: User) {
        
        users.append(user)
    }
    
    func updateUser(_ user: User) {
        
        if let index = users.firstIndex(where: { $0.id == user.id }) {
            users[index] = user
        }
    }
    
    func removeUser(_ indexPath: IndexPath) {
        
        DispatchQueue.main.async {
            self.users.remove(at: indexPath.row)
            self.userTableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    
    @IBAction func addUsersButtonPressed(_ sender: UIButton) {
        
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddUsersViewControlleID") as? AddUsersViewController else { return }
        controller.delegate = self
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension UserViewController: AddUsersViewControllerDelegate {
    
    func didFinishCreateUpdate(_ user: User) {
        
        if users.contains(where: { $0.id == user.id }) {
            updateUser(user)
        } else {
            saveUser(user)
        }
    }
}

