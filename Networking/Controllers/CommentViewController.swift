//
//  CommentViewController.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 7.02.22.
//

import UIKit

class CommentViewController: UIViewController {
    
    @IBOutlet weak var commentTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var networkManager = NetworkManager()
    var dataManager = DataManager()
    var currentPost = Post(userId: 0, title: "", body: "", id: 0)
    
    var comments = [Comment]() {
        didSet {
            DispatchQueue.main.async {
                self.commentTableView.reloadData()
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerCell()
        commentTableView.delegate = self
        commentTableView.dataSource = self
        configureActivityIndicator()
        dataManager.getCommentsBy(postId: currentPost.id) { [weak self] comments in
            self?.comments = comments
            if comments.isEmpty {
                self?.showAlertCommentsIsEmpty()
            }
        }
        configureRefreshControl()
    }
    
    func registerCell() {
        
        let nib = UINib(nibName: "CommentTableViewCell", bundle: nil)
        commentTableView.register(nib, forCellReuseIdentifier: "CommentTableViewCellID")
    }
    
    func configureActivityIndicator() {
        
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        activityIndicator.center = view.center
    }
    
    func showAlertCommentsIsEmpty () {
        
        let alert = UIAlertController(title: nil, message: "Your comments aren't there yet. You can create them", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func configureRefreshControl () {
        
        commentTableView.refreshControl = UIRefreshControl()
        commentTableView.refreshControl?.addTarget(self, action: #selector(handleRefreshControl), for: .valueChanged)
    }
    
    @objc func handleRefreshControl() {
        
        dataManager.getCommentsBy(postId: currentPost.id) { [weak self] comments in
            
            self?.comments = comments
            self?.commentTableView.refreshControl?.endRefreshing()
        }
    }
    
    func saveComment(_ comment: Comment) {
        
        comments.append(comment)
    }
    
    func updateComment(_ comment: Comment) {
        
        if let index = comments.firstIndex(where: { $0.id == comment.id }) {
            comments[index] = comment
        }
    }
    
    func removeComment(_ indexPath: IndexPath) {
        
        DispatchQueue.main.async {
            self.comments.remove(at: indexPath.row)
            self.commentTableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    @IBAction func createCommentButtonPressed(_ sender: Any) {
        
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddCommentViewControllerID") as? AddCommentViewController else { return }
        controller.delegate = self
        controller.selectedPost.id = currentPost.id
        navigationController?.pushViewController(controller, animated: true)
    }
}


extension CommentViewController: AddCommentViewControllerDelegate {
    
    func didFinishCreateUpdate(_ comment: Comment) {
        
        if comments.contains(where: { $0.id == comment.id }) {
            updateComment(comment)
        } else {
            saveComment(comment)
        }
    }
}
