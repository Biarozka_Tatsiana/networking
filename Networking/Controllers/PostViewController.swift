//
//  ViewController.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 27.01.22.
//

import UIKit

class PostViewController: UIViewController {
    
    @IBOutlet weak var postTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var users = [User]()
    var currentUser = User(id: 0, name: "", username: "", email: "", phone: "", website: "")
    var networkManager = NetworkManager()
    var dataManager = DataManager()
    var posts = [Post](){
        didSet {
            DispatchQueue.main.async {
                self.postTableView.reloadData()
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        postTableView.delegate = self
        postTableView.dataSource = self
        registerCell()
        configureActivityIndicator()
        dataManager.getPostsBy(userId: currentUser.id) {[weak self] posts in
            self?.posts = posts
            if posts.isEmpty {
                self?.showAlertPostsIsEmpty()
            }
        }
        configureRefreshControl()
    }
    
    func registerCell() {
        
        let nib = UINib(nibName: "PostTableViewCell", bundle: nil)
        postTableView.register(nib, forCellReuseIdentifier: "PostTableViewCellID")
    }
    
    func configureActivityIndicator() {
        
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        activityIndicator.center = view.center
    }
    
    func showAlertPostsIsEmpty () {
        
        let alert = UIAlertController(title: nil, message: "Your posts aren't there yet. You can create them", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func configureRefreshControl () {
        
        postTableView.refreshControl = UIRefreshControl()
        postTableView.refreshControl?.addTarget(self, action: #selector(handleRefreshControl), for: .valueChanged)
    }
    
    @objc func handleRefreshControl() {
        
        dataManager.getPostsBy(userId: currentUser.id) {[weak self] posts in
            self?.posts = posts
            self?.postTableView.refreshControl?.endRefreshing()
        }
    }
    
    func savePost(_ post: Post) {
        
        posts.append(post)
    }
    
    func updatePost(_ post: Post) {
        
        if let index = posts.firstIndex(where: { $0.id == post.id }) {
            posts[index] = post
        }
    }
    
    func removePost(_ indexPath: IndexPath) {
        
        DispatchQueue.main.async {
            self.posts.remove(at: indexPath.row)
            self.postTableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    @IBAction func createPost(_ sender: UIButton) {
        
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddPostsViewControllerID") as? AddPostsViewController else { return }
        controller.delegate = self
        controller.selectedUser.id = currentUser.id
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension PostViewController: AddPostsViewControllerDelegate {
    
    func didFinishCreateUpdate(_ post: Post) {
        
        if posts.contains(where: { $0.id == post.id }) {
            updatePost(post)
        } else {
            savePost(post)
        }
    }
}



