//
//  MainCoordinator.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 7.02.22.
//

import UIKit

class MainCoordinator: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func showUsersButtonPressed(_ sender: UIButton) {
        
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "UserViewControllerID") as? UserViewController else { return }
        navigationController?.pushViewController(controller, animated: true)
    }
    
}
