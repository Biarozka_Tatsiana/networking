//
//  PostEntity.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 9.02.22.
//

import Foundation
import CoreData


class PostEntity: NSManagedObject {
    
    class func findOrCreate(_ post: Post, context: NSManagedObjectContext) throws -> PostEntity? {
        
        if let postEntity = try PostEntity.find(post.id, context: context) {
            return postEntity
        } else {
            if let userEntity = try UserEntity.find(post.userId, context: context) {
                
                let entity = PostEntity(context: context)
                entity.id = Int64(post.id)
                entity.userId = Int64(post.userId)
                entity.title = post.title
                entity.body = post.body
                entity.user = userEntity
                return entity
            }
        }
        return nil
    }
    
    
    class func all(_ userId: Int,context: NSManagedObjectContext) throws -> [PostEntity]? {
        
        let request: NSFetchRequest<PostEntity> = PostEntity.fetchRequest()
        request.predicate = NSPredicate(format: "userId == %d", userId)
        
        do {
            let result = try context.fetch(request)
            return result
        } catch {
            throw error
        }
    }
    
    class func findAndUpdate(_ post: Post, context: NSManagedObjectContext) throws -> PostEntity? {
        
        do {
            if let postEntity = try PostEntity.find(post.id, context: context), let userEntity = try UserEntity.find(post.userId, context: context) {
                postEntity.title = post.title
                postEntity.body = post.body
                postEntity.user = userEntity
                return postEntity
            }
        } catch {
            throw error
        }
        return nil
    }
    
    class func findAndDelete(_ post: Post, context: NSManagedObjectContext) throws -> Void {
        
        do {
            if let postEntity = try PostEntity.find(post.id, context: context) {
                context.delete(postEntity)}
        } catch {
            throw error
        }
    }
    
    class func find(_ postId: Int, context: NSManagedObjectContext) throws -> PostEntity? {
        
        let request: NSFetchRequest<PostEntity> = PostEntity.fetchRequest()
        request.predicate = NSPredicate(format: "id = %d", postId)
        
        do {
            let fetchResult = try context.fetch(request)
            if fetchResult.count > 0 {
                assert(fetchResult.count == 1, "Duplicate has been found in DB")
                return fetchResult[0]
            }
        } catch {
            throw error
        }
        return nil
    }
}

