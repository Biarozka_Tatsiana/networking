//
//  CommentEntity.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 17.02.22.
//

import Foundation
import CoreData

class CommentEntity: NSManagedObject {
    
    class func findOrCreate(_ comment: Comment, context: NSManagedObjectContext) throws -> CommentEntity? {
        
        let request: NSFetchRequest <CommentEntity> = CommentEntity.fetchRequest()
        request.predicate = NSPredicate(format: "id == %d", comment.id)
        do {
            let result = try context.fetch(request)
            if result.count > 0 {
                assert(result.count == 1, "Duplicates in CommentEntity")
                return result[0]
            } else {
                if let postEntity = try PostEntity.find(comment.postId, context: context) {
                    
                    let entity = CommentEntity(context: context)
                    entity.postId = Int64(comment.postId)
                    entity.id = Int64(comment.id)
                    entity.name = comment.name
                    entity.body = comment.body
                    entity.email = comment.email
                    entity.post = postEntity
                    return entity
                }
            }
        } catch {
            throw error
        }
        return nil
    }
    
    class func all(_ postId: Int,context: NSManagedObjectContext) throws -> [CommentEntity]? {
        
        let request: NSFetchRequest<CommentEntity> = CommentEntity.fetchRequest()
        request.predicate = NSPredicate(format: "postId == %d", postId)
        
        do {
            let result = try context.fetch(request)
            return result
        } catch {
            throw error
        }
    }
    
    class func findAndUpdate(_ comment: Comment, context: NSManagedObjectContext) throws -> CommentEntity? {
        
        let request: NSFetchRequest <CommentEntity> = CommentEntity.fetchRequest()
        request.predicate = NSPredicate(format: "id == %d", comment.id)
        do {
            let result = try context.fetch(request)
            if result.count > 0 {
                assert(result.count == 1, "Duplicates in CommentEntity")
                if let postEntity = try PostEntity.find(comment.postId, context: context) {
                    
                    result[0].name = comment.name
                    result[0].body = comment.body
                    result[0].post = postEntity
                    return result[0]
                }
            }
        } catch {
            throw error
        }
        return nil
    }
    
    class func findAndDelete(_ comment: Comment, context: NSManagedObjectContext) throws -> Void {
        
        let request: NSFetchRequest <CommentEntity> = CommentEntity.fetchRequest()
        request.predicate = NSPredicate(format: "id == %d", comment.id)
        do {
            let result = try context.fetch(request)
            if result.count > 0 {
                assert(result.count == 1, "Duplicates in CommentEntity")
                context.delete(result[0])
            }
        } catch {
            throw error
        }
    }
}
