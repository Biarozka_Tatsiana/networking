//
//  UserEntity.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 2.02.22.
//

import Foundation
import CoreData

class UserEntity: NSManagedObject {
    
    class func findOrCreate(_ user: User, context: NSManagedObjectContext) throws -> UserEntity {
        
        if let userEntity = try? UserEntity.find(user.id, context: context) {
            
            return userEntity
        } else {
            
            let userEntity = UserEntity(context: context)
            userEntity.id = Int64(user.id)
            userEntity.name = user.name
            userEntity.username = user.username
            userEntity.email = user.email
            userEntity.phone = user.phone
            userEntity.website = user.website
            
            return userEntity
        }
    }
    
    class func findAndUpdate(_ user: User, context: NSManagedObjectContext) throws -> UserEntity? {
        
        do {
            if let userEntity = try UserEntity.find(user.id, context: context) {
                
                userEntity.name = user.name
                userEntity.username = user.username
                userEntity.email = user.email
                userEntity.phone = user.phone
                userEntity.website = user.website
                
                return userEntity
            }
            
        } catch {
            throw error
        }
        return nil
    }
    
    class func findAndDelete (_ user: User, context: NSManagedObjectContext) throws -> Void {
        
        do {
            if let userEntity = try UserEntity.find(user.id, context: context) {
                context.delete(userEntity)
            }
        } catch {
            
        }
    }
    
    class func all(context: NSManagedObjectContext) throws -> [UserEntity] {
        
        let request: NSFetchRequest<UserEntity> = UserEntity.fetchRequest()
        
        do {
            return try context.fetch(request)
            
        } catch {
            throw error
        }
    }
    
    class func find(_ userId: Int, context: NSManagedObjectContext) throws -> UserEntity? {
        
        let request: NSFetchRequest<UserEntity> = UserEntity.fetchRequest()
        request.predicate = NSPredicate(format: "id = %d", userId)
        
        do {
            let fetchResult = try context.fetch(request)
            if fetchResult.count > 0 {
                assert(fetchResult.count == 1, "Duplicate has been found in DB")
                return fetchResult[0]
            }
        } catch {
            throw error
        }
        return nil
    }
}
