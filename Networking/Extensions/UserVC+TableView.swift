//
//  UserVC+TableView.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 8.02.22.
//

import UIKit

extension UserViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCellID", for: indexPath) as? UserTableViewCell else { return UITableViewCell() }
        cell.configure(users[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let selectedUser = users[indexPath.row]
        
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { action, view, complete in
            
            self.dataManager.deleteUser(selectedUser) { _ in
                self.removeUser(indexPath)
            }
        }
        
        let editAction = UIContextualAction(style: .normal, title: "Edit") { action, view, complete in
            guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddUsersViewControlleID") as? AddUsersViewController else { return }
            controller.delegate = self
            controller.currentUser = selectedUser
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
        editAction.backgroundColor = .systemBlue
        deleteAction.backgroundColor = .systemRed
        let swipeAction = UISwipeActionsConfiguration(actions: [deleteAction,editAction])
        return swipeAction
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "PostViewControllerID") as? PostViewController else { return }
        let selectedUser = users[indexPath.row]
        controller.currentUser.id = selectedUser.id
        navigationController?.pushViewController(controller, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
