//
//  CommentVC+TableView.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 18.02.22.
//

import UIKit

extension CommentViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = commentTableView.dequeueReusableCell(withIdentifier: "CommentTableViewCellID", for: indexPath) as? CommentTableViewCell else { return UITableViewCell() }
        cell.configure(comments[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let selectedComment = comments[indexPath.row]
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { action, view, complete in
            self.dataManager.deleteComment(selectedComment) { _ in
                self.removeComment(indexPath)
            }
        }
        let editAction = UIContextualAction(style: .normal, title: "Edit") { action, view, complete in
            guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddCommentViewControllerID") as? AddCommentViewController else { return }
            controller.delegate = self
            controller.currentComment = selectedComment
            controller.selectedPost.id = selectedComment.postId
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
        editAction.backgroundColor = .systemBlue
        deleteAction.backgroundColor = .systemRed
        let swipeAction = UISwipeActionsConfiguration(actions: [deleteAction,editAction])
        return swipeAction
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
