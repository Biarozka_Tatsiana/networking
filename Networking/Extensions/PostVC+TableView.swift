//
//  PostVC+TableView.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 8.02.22.
//

import UIKit

extension PostViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCellID", for: indexPath) as? PostTableViewCell else { return UITableViewCell() }
        cell.configure(posts[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let selectedPost = posts[indexPath.row]
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { action, view, complete in
            self.dataManager.deletePost(selectedPost) { _ in
                self.removePost(indexPath)
            }
        }
        let editAction = UIContextualAction(style: .normal, title: "Edit") { action, view, complete in
            guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddPostsViewControllerID") as? AddPostsViewController else { return }
            controller.delegate = self
            controller.currentPost = selectedPost
            controller.selectedUser.id = self.currentUser.id
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
        editAction.backgroundColor = .systemBlue
        deleteAction.backgroundColor = .systemRed
        let swipeAction = UISwipeActionsConfiguration(actions: [deleteAction,editAction])
        return swipeAction
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "CommentViewControllerID") as? CommentViewController else { return }
        let selectedPost = posts[indexPath.row]
        controller.currentPost.id = selectedPost.id
        navigationController?.pushViewController(controller, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
