//
//  String+emailValidation.swift
//  Networking Belui
//
//  Created by Татьяна Березка on 8.02.22.
//

import UIKit

extension String {
    
    var isValidEmail: Bool {
        return NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}").evaluate(with: self)
    }
}
